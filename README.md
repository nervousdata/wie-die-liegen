## Wie die liegen

Bash script for generating a text collage by making oscillating cuts on a horizontal strip of an image.

A sine function defines the position where the cut is made and another sine function slightly changes the width of a snippet with every cut. The new image is composed by following the order of the cutting process; the first snippet produced is put on the left upper angle of the new page.

Needs [ImageMagick 7](https://imagemagick.org) installed.

### Examples

“nano” – Tiny snippets with max. width of 62 Pixel.

![wie-die-liegen_nano.png](./examples/wie-die-liegen_nano.png)

“micro” – Snippets with min. width of 1/20 of the original size (ca. 63-94 Pixel).

![wie-die-liegen_micro.png](./examples/wie-die-liegen_micro.png)

“mini” – Snippets with min. width of 1/7 of the original size (ca. 150-215 Pixel).

![wie-die-liegen_mini.png](./examples/wie-die-liegen_mini.png)
