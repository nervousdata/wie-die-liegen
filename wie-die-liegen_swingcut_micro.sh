#!/bin/bash
# Wie die liegen (oscillating cuts) – “micro”

ct=0
ft=2000 # frequency mod. higher = slower movement
am=2 # amplitude mod. higher = slower movement
until [ $ct -gt 527 ] # sets how often a cut will be made (+1) and how many snippets will be produced
do
  ((ct+=1)) # a counter. starting at 1, increasing by 1
  ctt=`printf %03d $ct` # prints 3 digits numbers, important for naming the files
  zw=`awk -v x="$ct" -v f=$ft -v a=$am 'BEGIN {wz=sin(5*(3+x*a))*sin(2*3.1416*(3+(x/f)))+0.9999; printf wz }'` # defines a sine function. shift 0.9999 above zero on y-axis
  sw=`awk -v x="$ct" 'BEGIN {wv=(sin(x*4)+5)*0.28; printf wv }'` # defines another sine function for slightly changing the width of snippets
  mkdir -p cuts pieces # creates directories for the cut images and the new images
  echo " Cutting …"
  magick kno-bae.png +profile "icc" -gravity SouthWest -crop "%[fx:(w/20)*$sw]"x"%[fx:h]"+"%[fx:(w/2.75)*$zw]"+0 +repage cuts/$ctt.png # cuts the image. the width of the snippets is set as a fraction of the width of the input image. the position on the x-axis defines where the cut is made. it is calculated with the values of the variable ‘zw’
done

while :
do
  echo ' Assembling ! '
  montage cuts/*.png -tile 16x -background white -geometry +0+0 -units PixelsPerInch -density 300 pieces/micro_am2_ft2000.png # composes a new image. with -tile the number of snippets in a row is defined. it should be a (integer) divisor of the number of snippets (line 7) to avoid gaps at the bottom of the new image
  break
done
