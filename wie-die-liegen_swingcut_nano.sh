#!/bin/bash
# Wie die liegen (oscillating cuts) – “nano”

ct=0
ft=5000 # frequency mod. higher = slower movement
am=500 # amplitude mod. higher = slower movement
until [ $ct -gt 923 ] # sets how often a cut will be made (+1) and how many snippets will be produced
do
  ((ct+=1)) # a counter. starting at 1, increasing by 1
  ctt=`printf %03d $ct` # prints 3 digits numbers, important for naming the files
  zw=`awk -v x="$ct" -v f="$ft" -v a=$am 'BEGIN {wz=sin(3*(4+x/a))*sin(2*3.1416*(4+(x/f)))+0.9999; printf wz }'` # defines a sine function. shift 0.9999 above zero on y-axis
  sw=`awk -v x="$ct" 'BEGIN {wv=0.88*sin(x*10-(3.1416*2))+2; printf wv }'` # defines another sine function for slightly changing the width of snippets
  mkdir -p cuts pieces # creates directories for the cut images and the new images
  echo " Cutting … "
  magick kno-bae.png +profile "icc" -gravity SouthWest -crop "%[fx:(w/52)*$sw]"x"%[fx:h]"+"%[fx:(w/3)*$zw]"+0 +repage cuts/$ctt.png # cuts the image. the width of the snippets is set as a fraction of the width of the input image. the position on the x-axis defines where the cut is made. it is calculated with the values of the variable ‘zw’
done

while :
do
  echo ' Assembling ! '
  montage cuts/*.png -tile 28x -background white -geometry +0+0 -units PixelsPerInch -density 300 pieces/nano_am500_ft5000.png # composes a new image. with -tile the number of snippets in a row is defined. it should be a (integer) divisor of ct+1 to avoid gaps at the bottom of the new image
  break
done
